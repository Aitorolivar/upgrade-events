// 1.1 Añade un botón a tu html con el id btnToClick y en tu javascript añade el evento click que ejecute un console log con la información del evento del click>


const $$btn = document.querySelector('#btnToClick');
$$btn.addEventListener('click', clickEvent);
function clickEvent(){
    console.log('Event click');
    const $$input = document.querySelector('.click');
    $$input.placeholder = 'Fill in field ';
};

// 1.2 Añade un evento 'focus' que ejecute un console.log con el valor del input.
const $$input2 = document.querySelector('.focus');
$$input2.addEventListener('focus', focusEvent);

function focusEvent(event){
    console.log('Focus in input');
    console.log(event) 
};

// 1.3 Añade un evento 'input' que ejecute un console.log con el valor del input.
const $$value = document.querySelector('.value');
$$value.addEventListener('input', PrintText);

function PrintText(event){
    let collectText = event.target.value; 
    console.log(collectText);
};

